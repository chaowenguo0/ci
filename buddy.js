import fetch from 'node-fetch'
import process from 'process'

const pipeline = await fetch('https://api.buddy.works/workspaces/chaowenguo/projects/ci/pipelines', {headers:{'authorization':`Bearer ${process.argv.at(2)}`}})
const execution = await fetch((await pipeline.json()).pipelines.at(0).url + '/executions', {headers:{'authorization':`Bearer ${process.argv.at(2)}`}})
console.log(await execution.json())
