import aiohttp, asyncio, base64, argparse, os
parser = argparse.ArgumentParser()
parser.add_argument('azurePipelines')

async def main():
    async with aiohttp.ClientSession() as client:
        async with client.get(f'{os.getenv("SYSTEM_COLLECTIONURI")}/{os.getenv("SYSTEM_TEAMPROJECT")}/_apis/build/builds', params={'api-version':'7.1'}) as response:
            for _ in (await response.json()).get('value'):
                if _.get('status') != 'inProgress':
                    async with client.delete(f'{os.getenv("SYSTEM_COLLECTIONURI")}/{os.getenv("SYSTEM_TEAMPROJECT")}/_apis/build/builds/{_.get("id")}', params={'api-version':'7.1'}, headers={'authorization':f'Basic {base64.b64encode((":" + parser.parse_args().azurePipelines).encode()).decode()}'} ) as _: pass

asyncio.run(main())
#curl -u :$2 -H content-type:application/json -d '{}' https://dev.azure.com/chaowenguo/ci/_apis/pipelines/24/runs?api-version=7.1